package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.Empleado;

public interface IEmpleadoService {

    //Metodos del CRUD
        public List<Empleado> listarEmpleado(); //Listar All 

        public Empleado guardarEmpleado(Empleado empleado);    //Guarda un empleado CREATE

        public Empleado EmpleadoXID(Long id); //Leer datos de un empleado READ

        public Empleado actualizarEmpleado(Empleado empleado); //Actualiza datos del empleado UPDATE

        public void eliminarEmpleado(Long id);// Elimina el empleado DELETE
}